import { Directive, ElementRef, Renderer2, OnInit } from '@angular/core';

@Directive({
selector: '[appRendererHighlight]'
})
export class RendererHighlightDirective implements OnInit {

constructor(private renderer : Renderer2, private elementRef : ElementRef) { }

    ngOnInit() {
        this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', 'lightblue');
    }

}

