import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
selector: '[appHostBinding]'
})
export class HostBindingDirective {

constructor() { }

  @HostBinding('style.backgroundColor')
  bgColor = 'transparent';

  @HostListener('mouseenter')
    onMouseEnter(event : Event) {
      this.bgColor = 'cyan';
    }
      @HostListener('mouseleave')
    onMouseLeave(event : Event) {
      this.bgColor = 'transparent';
    }

}
