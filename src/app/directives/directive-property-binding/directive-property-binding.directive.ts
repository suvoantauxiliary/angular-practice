import { Directive, OnInit, Input, HostListener, HostBinding } from '@angular/core';

@Directive({
selector: '[appDirectivePropertyBinding]'
})
export class DirectivePropertyBindingDirective implements OnInit {

@Input()
defaultColor : string='';

@Input()
highlightColor : string='';

constructor() { }

    ngOnInit() {
        this.defaultColor = 'transparent';
    }

    @HostBinding('style.backgroundColor')
      bgColor = 'transparent';

    @HostListener('mouseenter')
        onMouseEnter(event : Event) {
        this.bgColor = this.highlightColor;
    }
    @HostListener('mouseleave')
    onMouseLeave(event : Event) {
        this.bgColor = this.defaultColor;
    }
}
