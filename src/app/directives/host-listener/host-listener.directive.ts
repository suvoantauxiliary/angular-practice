import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
selector: '[appHostListener]'
})
export class HostListenerDirective {

constructor(private renderer : Renderer2, private elementRef : ElementRef) { }

  @HostListener('mouseenter')
  onMouseEnter(event : Event) {
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', 'orange');
  }
    @HostListener('mouseleave')
  onMouseLeave(event : Event) {
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', 'transparent');
  }
}
