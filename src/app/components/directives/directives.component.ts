import { Component,OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
selector: 'app-directives-component',
templateUrl: './directives.component.html',
styleUrl: './directives.component.css'
})
export class DirectivesComponent implements OnInit {

type : string = '';
constructor(private route : ActivatedRoute) {}

    ngOnInit() {

    this.route.queryParams.subscribe(
        (params) => {

            this.type = params['type'];
        });
    }
}
