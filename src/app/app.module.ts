import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { BasicHighlightDirective } from './directives/basic-highlight/basic-highlight.directive';
import { DirectivesComponent } from './components/directives/directives.component';
import { RendererHighlightDirective } from './directives/renderer-highlight/renderer-highlight.directive';
import { HostListenerDirective } from './directives/host-listener/host-listener.directive';
import { HostBindingDirective } from './directives/host-binding/host-binding.directive';
import { DirectivePropertyBindingDirective } from './directives/directive-property-binding/directive-property-binding.directive';

@NgModule({
declarations: [
AppComponent,
HomeComponent,
BasicHighlightDirective,
DirectivesComponent,
RendererHighlightDirective,
HostListenerDirective,
HostBindingDirective,
DirectivePropertyBindingDirective
],
imports: [
BrowserModule,
AppRoutingModule
],
providers: [],
bootstrap: [AppComponent]
})
export class AppModule { }
